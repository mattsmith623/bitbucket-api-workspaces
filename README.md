# PHP Bitbucket API - Workspaces Polyfill

Workspaces polyfill for [PHP Bitbucket API](https://bitbucket.org/gentlero/bitbucket-api/) by [Gentle Software](https://bitbucket.org/gentlero/)

## Requirements

* PHP >= 5.6.0 with [cURL](http://php.net/manual/en/book.curl.php) extension.
* [PHP Bitbucket API](https://bitbucket.org/gentlero/bitbucket-api/) library.

## License

`bitbucket-api` is licensed under the MIT License - see the LICENSE file for details
